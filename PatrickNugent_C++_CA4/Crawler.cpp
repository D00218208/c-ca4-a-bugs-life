#include "Crawler.h"

Crawler::Crawler(int id, pair<int, int> position, int direction, int size, bool alive, list<pair<int, int>> path) : Bug(id, position, direction, size, alive, path)
{

}

/*
* Moves the crawler according to the specified rules. Calls isWayBlocked()
* to check if their path is blocked and changes direction if true.
*/
void Crawler::move()
{
	pair<int, int> position = this->getPosition();
	int direction = this->getDirection();
	int newDirection;
	list<pair<int, int>> path = this->getPath();

	if (isWayBlocked(1))
	{
		newDirection = (rand() % 4) + 1;
		this->setDirection(newDirection);

		while (isWayBlocked(1))
		{
			newDirection = (rand() % 4) + 1;
			this->setDirection(newDirection);
		}
		if (this->getDirection() == 1)
		{
			position.second = position.second + 1;
		}
		else if (this->getDirection() == 2)
		{
			position.first = position.first + 1;
		}
		else if (this->getDirection() == 3)
		{
			position.second = position.second - 1;
		}
		else
		{
			position.first = position.first - 1;
		}
		this->setPosition(position);
		this->setDirection(newDirection);
		path.push_back(position);
		this->setPath(path);
	}
	else
	{
		if (direction == 1)
		{
			position.second = position.second + 1;
		}
		else if (direction == 2)
		{
			position.first = position.first + 1;
		}
		else if (direction == 3)
		{
			position.second = position.second - 1;
		}
		else
		{
			position.first = position.first - 1;
		}
		this->setPosition(position);
		path.push_back(position);
		this->setPath(path);
	}
}