#pragma once
#include "Bug.h"
class Hopper : public Bug
{
	int hopLength;
public:
	Hopper(int id, pair<int, int> position, int direction, int size, bool alive, list<pair<int, int>> path, int hopLength);
	void move();

	int getHopLength();

	void setHopLength(int hopLength);
};