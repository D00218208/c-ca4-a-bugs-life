#include "Hopper.h"

Hopper::Hopper(int id, pair<int, int> position, int direction, int size, bool alive, list<pair<int, int>> path, int hopLength) : Bug(id, position, direction, size, alive, path)
{
	this->hopLength = hopLength;
}

int Hopper::getHopLength()
{
	return this->hopLength;
}

void Hopper::setHopLength(int hopLength)
{
	this->hopLength = hopLength;
}

/*
* Moves the hopper according to the specified rules. Calls isWayBlocked()
* to check if they can move their full hop length and determines if the
* hopper hits a wall or changes direction.
*/
void Hopper::move()
{
	pair<int, int> position = this->getPosition();
	int direction = this->getDirection();
	int newDirection;
	int hopLength = this->getHopLength();
	list<pair<int, int>> path = this->getPath();

	if (isWayBlocked(1))
	{
		newDirection = (rand() % 4) + 1;
		this->setDirection(newDirection);

		while (isWayBlocked(1))
		{
			newDirection = (rand() % 4) + 1;
			this->setDirection(newDirection);
		}

		if (isWayBlocked(hopLength))
		{
			if (this->getDirection() == 1)
			{
				position.second = 10;
			}
			else if (this->getDirection() == 2)
			{
				position.first = 10;
			}
			else if (this->getDirection() == 3)
			{
				position.second = 0;
			}
			else
			{
				position.first = 0;
			}
			this->setPosition(position);
			this->setDirection(newDirection);
			path.push_back(position);
			this->setPath(path);
		}
		else
		{
			if (this->getDirection() == 1)
			{
				position.second = position.second + hopLength;
			}
			else if (this->getDirection() == 2)
			{
				position.first = position.first + hopLength;
			}
			else if (this->getDirection() == 3)
			{
				position.second = position.second - hopLength;
			}
			else
			{
				position.first = position.first - hopLength;
			}
			this->setPosition(position);
			path.push_back(position);
			this->setPath(path);
		}
	}
	else
	{
		if (isWayBlocked(hopLength))
		{
			if (direction == 1)
			{
				position.second = 10;
			}
			else if (direction == 2)
			{
				position.first = 10;
			}
			else if (direction == 3)
			{
				position.second = 0;
			}
			else
			{
				position.first = 0;
			}
			this->setPosition(position);
			path.push_back(position);
			this->setPath(path);
		}
		else
		{
			if (direction == 1)
			{
				position.second = position.second + hopLength;
			}
			else if (direction == 2)
			{
				position.first = position.first + hopLength;
			}
			else if (direction == 3)
			{
				position.second = position.second - hopLength;
			}
			else
			{
				position.first = position.first - hopLength;
			}
			this->setPosition(position);
			path.push_back(position);
			this->setPath(path);
		}
	}
}
