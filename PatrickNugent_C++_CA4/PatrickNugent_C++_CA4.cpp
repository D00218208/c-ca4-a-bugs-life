#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>  
#include <vector>
#include <algorithm>  
#include "Bug.h"
#include "Hopper.h"
#include "Crawler.h"
using namespace std;
void displayBugs();
void findBug();
void tapBoard();
void displayHistory();
void displayCells();
void printChoices();
void parseLine(const string& str);
void readBugs();
void writeBugs();
void fight();

vector<Bug*> bugs;

int main()
{
    readBugs();

    printChoices();

    bool finished = false;

    while (finished == false)
    {
        int choice;

        cout << "\nEnter a choice (enter 6 for list): ";
        cin >> choice;

        switch (choice)
        {
        case 1:
            displayBugs();
            break;
        case 2:
            findBug();
            break;
        case 3:
            tapBoard();
            break;
        case 4:
            displayHistory();
            break;
        case 5:
            displayCells();
            break;
        case 6:
            printChoices();
            break;
        case 7:
            writeBugs();
            finished = true;
            continue;
        default:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "Please enter one of the listed choices" << endl;
            break;
        }
    }
}

/*
* Iterates through the vector of Bug objects and prints the details of each.
*/
void displayBugs()
{
    int count = 0;
    string type;
    int directionValue;
    string direction;
    bool alive;
    string status;

    cout << "id, type, location, size, direction, hopLength, status\n" << endl;
    vector<Bug*>::iterator iter = bugs.begin();
    for (iter = bugs.begin(); iter != bugs.end(); iter++)
    {
        pair<int, int> position = (*iter)->getPosition();
        int xCoordinate = position.first;
        int yCoordinate = position.second;

        directionValue = (*iter)->getDirection();
        if (directionValue == 1)
        {
            direction = "North";
        }
        else if (directionValue == 2)
        {
            direction = "East";
        }
        else if (directionValue == 3)
        {
            direction = "South";
        }
        else
        {
            direction = "West";
        }

        alive = (*iter)->isAlive();
        if (alive == true)
        {
            status = "Alive";
        }
        else
        {
            status = "Dead";
        }

        count++;

        Hopper* hopper = (Hopper*)*iter;
        if ((hopper)->getHopLength() > 0)
        {
            type = "Hopper";       
            cout << (*iter)->getId() << ", " << type << ", (" << xCoordinate << "," << yCoordinate << "), " << (*iter)->getSize() << ", " << direction << ", " << (hopper)->getHopLength() << ", " << status << endl;
        }
        else
        {
            type = "Crawler";
            cout << (*iter)->getId() << ", " << type << ", (" << xCoordinate << "," << yCoordinate << "), " << (*iter)->getSize() << ", " << direction << ", " << status << endl;
        }

    }

    if (count == 0)
    {
        cout << "No bugs have been found" << endl;
    }
}

/*
* Prompts the user to enter the id of a bug and displays its details
*/
void findBug()
{
    int count = 0;
    string type;
    int directionValue;
    string direction;
    bool alive;
    string status;
    int id;

    cout << "Enter the ID of the bug you want to find: ";
    cin >> id;

    while (cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The ID entered is not a number, please re-enter: ";
        cin >> id;
    }

    cout << "\nid, type(location), size, direction, hopLength, status\n" << endl;
    vector<Bug*>::iterator iter = bugs.begin();
    for (iter = bugs.begin(); iter != bugs.end(); iter++)
    {
        if ((*iter)->getId() == id)
        {
            count++;
            pair<int, int> position = (*iter)->getPosition();
            int xCoordinate = position.first;
            int yCoordinate = position.second;

            directionValue = (*iter)->getDirection();
            if (directionValue == 1)
            {
                direction = "North";
            }
            else if (directionValue == 2)
            {
                direction = "East";
            }
            else if (directionValue == 3)
            {
                direction = "South";
            }
            else
            {
                direction = "West";
            }

            alive = (*iter)->isAlive();
            if (alive == true)
            {
                status = "Alive";
            }
            else
            {
                status = "Dead";
            }

            Hopper* hopper = (Hopper*)*iter;
            if ((hopper)->getHopLength() > 0)
            {
                type = "Hopper";
                cout << (*iter)->getId() << ", " << type << "(" << xCoordinate << "," << yCoordinate << "), " << (*iter)->getSize() << ", " << direction << ", " << (hopper)->getHopLength() << ", " << status << endl;
            }
            else
            {
                type = "Crawler";
                cout << (*iter)->getId() << ", " << type << "(" << xCoordinate << "," << yCoordinate << "), " << (*iter)->getSize() << ", " << direction << ", " << status << endl;
            }
        }
    }

    if (count == 0)
    {
        cout << "No bug with that ID has been found" << endl;
    }
}

/*
* Iterates through the vector of Bug objects and calls the move() function
* on each bug as long as they are still alive.
*/
void tapBoard()
{
    string type;
    int directionValue;
    string direction;
    bool alive;
    string status;
    int id;

    vector<Bug*>::iterator iter = bugs.begin();
    for (iter = bugs.begin(); iter != bugs.end(); iter++)
    {
        Hopper* hopper = (Hopper*)*iter;
        if (((hopper)->getHopLength() > 0) && hopper->isAlive())
        {
            hopper->move();
        }
        else
        {
           Crawler* crawler = (Crawler*)*iter;
           if (crawler->isAlive())
           {
               crawler->move();
           }
        }
    }
    fight();
}

/*
* Called after tapBoard() has finished a round of movement. This function checks
* what bugs are on the same cell and determines who the winner is. The winning
* bug increases in size while the losing bug is declared dead and can no longer move.
*/
void fight()
{
    vector<Bug*>::iterator iter = bugs.begin();
    vector<Bug*>::iterator iter2 = bugs.begin();

    cout << endl;

    for (iter = bugs.begin(); iter != bugs.end(); iter++)
    {
        for (iter2 = bugs.begin(); iter2 != bugs.end(); iter2++)
        {
            if (((*iter)->getPosition()) == ((*iter2)->getPosition()) && ((*iter)->isAlive()) && ((*iter2)->isAlive()))
            {
                int size1 = (*iter)->getSize();
                int size2 = (*iter2)->getSize();
                string id1 = to_string((*iter)->getId());
                string id2 = to_string((*iter2)->getId());

                if (((*iter)->getSize()) > ((*iter2)->getSize()))
                {
                    cout << "Bug " << (*iter)->getId() << " has eaten bug " << (*iter2)->getId() << "!" << endl;
                    (*iter)->setSize(size1 + size2);
                    (*iter2)->setAlive(false);
                    (*iter2)->status = "Eaten by " + id1;
                }
                else if (((*iter2)->getSize()) > ((*iter)->getSize()))
                {
                    cout << "Bug " << (*iter2)->getId() << " has eaten bug " << (*iter)->getId() << "!" << endl;
                    (*iter2)->setSize(size1 + size2);
                    (*iter)->setAlive(false);
                    (*iter)->status = "Eaten by " + id2;
                }
                else
                {

                }
            }
        }
    }
}

/*
* Iterates through the vector of Bug objects and lists the path taken 
* by each bug as well as indicating whether the bug is alive or dead
*/
void displayHistory()
{
    int count = 0;
    string type;
    int directionValue;
    string direction;
    bool alive;
    string status;
    list<pair<int, int>> path;

    vector<Bug*>::iterator iter = bugs.begin();
    for (iter = bugs.begin(); iter != bugs.end(); iter++)
    {
        alive = (*iter)->isAlive();
        if (alive == true)
        {
            status = "Alive";
        }
        else
        {
            status = (*iter)->status;
        }

        count++;

        path = (*iter)->getPath();
        Hopper* hopper = (Hopper*)*iter;
        if ((hopper)->getHopLength() > 0)
        {
            type = "Hopper";
        }
        else
        {
            type = "Crawler";
        }
        cout << (*iter)->getId() << ", " << type << ", Path: ";

        list<pair<int,int>>::iterator listIter = path.begin();
        for (listIter = path.begin(); listIter != path.end(); listIter++)
        {
           cout << "(" << (*listIter).first << "," << (*listIter).second << "),";
        }
        cout << " " << status << endl;
    }
    if (count == 0)
    {
        cout << "No bugs have been found" << endl;
    }
}

/*
* Display each cell of the board and lists the bugs that are occupying
* each cell.
*/
void displayCells()
{
    for (int i = 0; i <= 10; i++)
    {
        for (int j = 0; j <= 10; j++)
        {
            cout << "(" << i << "," << j << "): ";

            vector<Bug*>::iterator iter = bugs.begin();
            int count = 0;
            for (iter = bugs.begin(); iter != bugs.end(); iter++)
            {
                if (((*iter)->getPosition().first == i) && ((*iter)->getPosition().second == j))
                {
                    if (count != 0)
                    {
                        cout << ", ";
                    }
                    count++;
                    string type;
                    Hopper* hopper = (Hopper*)*iter;
                    if ((hopper)->getHopLength() > 0)
                    {
                        type = "Hopper";
                    }
                    else
                    {
                        type = "Crawler";
                    }
                    cout << type << " " << (*iter)->getId();
                }
            }
            if (count == 0)
            {
                cout << "empty" << endl;
            }
            else
            {
                cout << endl;
            }        
        }
    }
}

/*
* Parses each line of a text file by gathering each attribute and
* using them to create either a Hopper or Crawler object.
*/
void parseLine(const string& str)
{
    stringstream strStream(str);
    int id = 0;
    int xCoordinate = 0;
    int yCoordinate = 0;
    int direction = 0;
    int size = 0;
    int hopLength = 0;

    try
    {
        string type;
        getline(strStream, type, ';');

        string str;
        getline(strStream, str, ';');

        id = stoi(str);

        getline(strStream, str, ';');

        xCoordinate = stoi(str);

        getline(strStream, str, ';');

        yCoordinate = stoi(str);

        getline(strStream, str, ';');

        direction = stoi(str);

        getline(strStream, str, ';');

        size = stoi(str);

        pair<int, int> position = { xCoordinate, yCoordinate };

        list<pair<int, int>> path;
        path.push_back(position);

        if (type == "H")
        {
            getline(strStream, str, '\n');

            hopLength = stoi(str);

            Hopper *hopper = new Hopper(id, position, direction, size, true, path, hopLength);
            bugs.push_back(hopper);
        }
        else
        {
            Crawler *crawler = new Crawler(id, position, direction, size, true, path);
            bugs.push_back(crawler);
        }
    }
    catch (std::invalid_argument const& e)
    {
        cout << "Bad input: std::invalid_argument thrown" << '\n';
    }
    catch (std::out_of_range const& e)
    {
        cout << "Integer overflow: std::out_of_range thrown" << '\n';
    }
}

/*
* Opens a text file of bugs, parses each line into a bug
* object and adds them to the vector of pointers to bug objects.
*/
void readBugs()
{
    cout << "Reading from bugs.txt" << endl;

    string line;
    ifstream inStream("bugs.txt");

    if (inStream.good())
    {
        while (getline(inStream, line))
        {
            parseLine(line);
        }
        inStream.close();
        //sort(bugs.begin(), bugs.end(), compareIDAscending);
        cout << "Bugs loaded from file successfully" << endl;
    }
    else
    {
        cout << "Unable to open file, or file is empty";
    }
}

/*
* Iterates through the vector of Bug objects, gathers the life history
* of each bug and writes it to a file, line by line.
*/
void writeBugs()
{
    cout << "Creating and writing to file: bugs_life_out.txt" << endl;

    ofstream outStream("bugs_life.out.txt");

    if (outStream.good())
    {
        int count = 0;
        string type;
        int directionValue;
        string direction;
        bool alive;
        string status;
        list<pair<int, int>> path;

        vector<Bug*>::iterator iter = bugs.begin();
        for (iter = bugs.begin(); iter != bugs.end(); iter++)
        {
            string output;
            alive = (*iter)->isAlive();
            string id = to_string((*iter)->getId());
            if (alive == true)
            {
                status = "Alive";
            }
            else
            {
                status = (*iter)->status;
            }

            count++;

            path = (*iter)->getPath();
            Hopper* hopper = (Hopper*)*iter;
            if ((hopper)->getHopLength() > 0)
            {
                type = "Hopper";
            }
            else
            {
                type = "Crawler";
            }
            
            output = id + ", " + type + ", Path: ";

            list<pair<int, int>>::iterator listIter = path.begin();
            for (listIter = path.begin(); listIter != path.end(); listIter++)
            {
                string xCoordinate = to_string((*listIter).first);
                string yCoordinate = to_string((*listIter).second);
                output = output + "(" + xCoordinate + "," + yCoordinate + "),";
            }
            output = output + " " + status;
            outStream << output << endl;
        }

        outStream.close();
        cout << "Bugs life history written to file successfully" << endl;
    }
    else
    {
        cout << "Unable to open file";
    }
}

/*
* Prints a list of choices available to the user while they
* are using the menu.
*/
void printChoices()
{
    cout << "\n1 - Display all bugs" << endl;
    cout << "2 - Find a bug" << endl;
    cout << "3 - Tap the bug board" << endl;
    cout << "4 - Display life history of all bugs" << endl;
    cout << "5 - Display all cells" << endl;
    cout << "6 - Print list of available choices again" << endl;
    cout << "7 - Exit and save life history of bugs" << endl;
}