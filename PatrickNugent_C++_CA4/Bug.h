#pragma once
#include <iostream>
#include <string>
#include <list>
using namespace std;
class Bug
{
protected:
	int id;
	pair<int, int> position;
	int direction;
	int size;
	bool alive;
	list<pair<int, int>> path;
public:
	string status;
	Bug(int id, pair<int, int> position, int direction, int size, bool alive, list<pair<int, int>> path);
	virtual void move() = 0;
	bool isWayBlocked(int amount);

	int getId();
	pair<int, int> getPosition();
	int getDirection();
	int getSize();
	bool isAlive();
	list<pair<int, int>> getPath();

	void setId(int id);
	void setPosition(pair<int, int> position);
	void setDirection(int direction);
	void setSize(int size);
	void setAlive(bool alive);
	void setPath(list<pair<int, int>>);
};
