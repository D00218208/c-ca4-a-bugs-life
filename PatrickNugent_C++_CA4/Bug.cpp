#include "Bug.h"

string status;

Bug::Bug(int id, pair<int, int> position, int direction, int size, bool alive, list<pair<int, int>> path)
{
	this->id = id;
	this->position = position;
	this->direction = direction;
	this->size = size;
	this->alive = alive;
	this->path = path;
}


int Bug::getId()
{
	return this->id;
}

pair<int, int> Bug::getPosition()
{
	return this->position;
}

int Bug::getDirection()
{
	return this->direction;
}

int Bug::getSize()
{
	return this->size;
}

bool Bug::isAlive()
{
	return this->alive;
}

list<pair<int, int>> Bug::getPath()
{
	return this->path;
}

void Bug::setId(int id)
{
	this->id = id;
}

void Bug::setPosition(pair<int, int> position)
{
	this->position = position;
}

void Bug::setDirection(int direction)
{
	this->direction = direction;
}

void Bug::setSize(int size)
{
	this->size = size;
}

void Bug::setAlive(bool alive)
{
	this->alive = alive;
}

void Bug::setPath(list<pair<int, int>> path)
{
	this->path = path;
}

/*
* Returns true if the bug can't move the specified amount in their
* current direction, otherwise returns false.
*/
bool Bug::isWayBlocked(int amount)
{
	int direction = this->getDirection();
	pair<int, int> position = this->getPosition();
	int xCoordinate = position.first;
	int yCoordinate = position.second;

	if ((direction == 1) && (yCoordinate + amount > 10))
	{
		return true;
	}
	else if ((direction == 3) && (yCoordinate - amount < 0))
	{
		return true;
	}
	else if ((direction == 2) && (xCoordinate + amount > 10))
	{
		return true;
	}
	else if ((direction == 4) && (xCoordinate - amount < 0))
	{
		return true;
	}
	else
	{
		return false;
	}
	return false;
}